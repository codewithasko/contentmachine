const fs = require('fs')
const fm = require('front-matter')
const marked = require('marked')

class ContentMachine {

	directory = '';
	parseMarkdown = true;
	orderBy = false;
	order = false;
	limit = 10;
	offset = 0;

	constructor({directory = false, parseMarkdown, orderBy = false, order = false, limit = false, offset = false}) {

    if(directory) this.directory = directory;
		if(orderBy) this.orderBy = orderBy;
		if(order) this.order = order;
		if(limit) this.limit = limit;
    if(offset) this.offset = offset;
    
    if(typeof parseMarkdown !== 'undefined') {

      this.parseMarkdown = parseMarkdown;

    }
	}

	/**
	 * Retrieve content files from a specified directory
	 */
	_getContentFiles() {

		return new Promise((resolve, reject) => {

			fs.readdir(this.directory, (err, files) => {

				if(err) {

					console.log('Unable to scan directory: ' + this.directory);
					resolve([]);

				}

				if(files) {

					var markdownFiles = [];
					var fileCounter = files.length;

					files.forEach(file => {

						if(file.endsWith('.md')) {

							markdownFiles.push(file);

						}

						fileCounter--;

						// All done?
						if(fileCounter == 0) {

							resolve(markdownFiles);

						}

					})

				} else {

					console.log('No files were found.');
					resolve([]);

				}

			})

		})

	}

	/**
	 * Parse content files
	 */
	_parseContentFilesIntoItems() {

		return new Promise((resolve, reject) => {

			this._getContentFiles().then(files => {

				var items = [];
				var fileCounter = files.length;

				if(files.length) {

					files.forEach(file => {

						fs.readFile(this.directory + '/' + file, 'utf8', (err, contents) => {

							if(err) {

								reject('Something went wrong trying to read the file: ' + file);

							}

							// Parse data on file
							let parsedContent = fm(contents)

              console.log(this.parseMarkdown);

							// Add to items
							items.push({
								meta: parsedContent.attributes,
								content: this.parseMarkdown ? marked(parsedContent.body) : parsedContent.body
							})

							fileCounter--;

							// All done?
							if(fileCounter == 0) {

								resolve(items);

							}

						})

					})

				} else {

					resolve([]);

				}

			}).catch(err => {

				console.log('caught', err);
				resolve([]);

			})

		})

	}

	/**
	 * Sort items by meta key, in descending order
	 */
	_sortItemsByKeyDesc(key, items) {

		return items.sort((a, b) => (a.meta[key] < b.meta[key] ? 1 : -1));

	}

	/**
	 * Sort items by meta key, in ascending order
	 */
	_sortItemsByKeyAsc(key, items) {

		return items.sort((a, b) => (a.meta[key] > b.meta[key] ? 1 : -1));

	}

	/**
	 * Get content items
	 */
	items() {

		// Are we ordering?
		if(this.order && this.orderBy) {

			return new Promise((resolve, reject) => {

				this._parseContentFilesIntoItems().then(items => {

					if(this.order === 'asc') {

						resolve(this._sortItemsByKeyAsc(this.orderBy, items).slice(this.offset, this.limit));

					} else {

						resolve(this._sortItemsByKeyDesc(this.orderBy, items).slice(this.offset, this.limit));

					}

				}).catch(err => {

					console.log('caught', err);

				})

			})

		// Otherwise return the items as they came
		} else {

			return new Promise((resolve, reject) => {

				this._parseContentFilesIntoItems().then(items => {

					resolve(items.slice(this.offset, this.limit));

				}).catch(err => {

					console.log('caught', err);

				})

			})

		}

	}

	/**
	 * Get content item
	 */
	item({key = false, value = false}) {

		return new Promise((resolve, reject) => {

			if(key && value) {

				this._parseContentFilesIntoItems().then(items => {

					resolve(items.filter(item => item.meta[key] === value)[0]);

				}).catch(err => {

					console.log('caught', err)
					reject(err);

				})

			} else {

				reject('No key or value present.')

			}

		})

	}

	}

module.exports = ContentMachine
