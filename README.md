# ContentMachine

**ContentMachine** is a static content engine written in Javascript, for Node.js projects. It powers my [personal site](https://askonomm.com) and is meant to be used for sites with static content, such as blogs or websites, either as a complimentary addition or the sole thing driving it - that's for you to decide.

**Note:** for any issues please refer to the [ticket tracker](https://todo.sr.ht/~askonomm/contentmachine).

### Example content file

All of the content files in **ContentMachine** are Markdown files. That means they have a file name of `example.md`. They can be in any directory you want (you can specify it) and they contain YAML metadata. An example of a ContentMachine file is the following (probably a familiar format if you've used Jekyll before):

```
---
date: 2019-02-18
status: public
slug: example-url-slug
title: Example page
---

Example content in **Markdown** goes here.
```

This would create the following object:

```Javascript
{
  meta: {
    date: "2019-02-18",
    status: "public",
    slug: "example-url-slug",
    title: "Example page"
  },
  content: "<p>Example content in <em>Markdown</em> goes here.</p>"
}
```

### Installation

To install ContentMachine, get it from NPM:

```
npm install contentmachine --save
```

And then simply require it:

```Javascript
const contentmachine = require('contentmachine')
```

### Usage

#### Retrieving all content in a directory

To retrieve all of the content in a directory, simply initialize ContentMachine with your provided directory and call `items()` on it, like this:

```Javascript
const content = new contentmachine({directory: "./Blog/"})

content.items().then(items => {

  // access your items here

})
```

This will return you an array of objects.

#### Retreving a specific item in a directory

To retrieve a specific content item in a directory, initialize ContentMachine and call `item({key: "key", value: "value"})` on it (where key is corresponding with the YAML meta key and the value with its value in the file), like this:

```Javascript
const content = new contentmachine({directory: "./Blog/"})

content.item({key: "slug", value: "hello-world"}).then(item => {

  // access your item here

})
```

#### Ordering content

You can order content by any meta key in descending or ascending order. For example; if we want to order content by the meta key `date` in a descending order, we would do the following:

```Javascript
const content = new contentmachine({directory: "./Blog/", orderBy: "date", order: "desc"})
```

Likewise if you want it to be in ascending order, simply change `order` to `asc`.

#### Limiting and offsetting content

You can limit and offset content by adding `limit` and/or `offset` to the constructor. For example:

```Javascript
const content = new contentmachine({directory: "./Blog", limit: 100, offset: 50})
```

Note that if you don't specify limit or offset, then they fall back to their default values 10 and 0, respectively.

#### Turning off Markdown parsing

There might be a use case where you don't want the body of the document to be parsed into HTML from Markdown and you wish to get plaintext instead. In that case, simply pass `parseMarkdown: false` to the constructor of ContentMachine, like so:

```JavaScript
const content = new contentmachine({directory: "./Blog", parseMarkdown: false});
```